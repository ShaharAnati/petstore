﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

var canvas = document.getElementById('viewport'),
    context = canvas.getContext('2d');

make_base();

function make_base() {
    base_image = new Image();
    console.log(window.location.pathname)
    base_image.src = "../Content/Images/logo2.png";
    base_image.onload = function () {
        context.drawImage(base_image, 0, 0);
    }
}