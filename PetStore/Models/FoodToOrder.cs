﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PetStore.Models
{
    public class FoodToOrder
    {
        [Key]
        public int OrderId { get; set; }
        public Order Order { get; set; }

        [Key]
        public int FoodId { get; set; }
        public Food Food { get; set; }

      
        public int Quantity { get; set; }
    }
}
