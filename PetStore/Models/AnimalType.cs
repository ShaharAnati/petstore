﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class AnimalType
    {
        public string Id { get; set; }

        public List<Animal> Animals { get; set; }
    }
}
