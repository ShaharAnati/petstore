﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PetStore.Models
{
    [NotMapped]
    public class OrderItem
    {

        public OrderItem(string Name, byte[] Image, double Price, int? Quantity)
        {
            this.Name = Name;
            this.Image = Image;
            this.Price = Price;
            this.Quantity = Quantity;
        }

        public string Name { get; set; }
        public byte[] Image { get; set; }
        public double Price { get; set; }

        public int? Quantity;
    }
}
