﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class AnimalFoodType
    {
        public int AnimalTypeId { get; set; }

        public int FoodId { get; set; }
    }
}
