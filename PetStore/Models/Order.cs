﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PetStore.Models
{
    public class Order
    {

        public int Id { get; set; }

        [Display(Name = "Creation date")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Username")]
        public string UserName { get; set; }

        [ForeignKey("UserName")]
        public User User { get; set; }
        public List<Animal> Animals { get; set; }
        public List<Food> Foods { get; set; }

        [NotMapped]
        [Display(Name = "Total price")]
        public double TotalPrice { get; set; }
        public List<FoodToOrder> FoodsToOrder { get; set; }

    }
}
