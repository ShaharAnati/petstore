﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class CartItem
    {

        public class ProductData
        {
            public byte[] Image;
            public double Price;
            public int Id;
            public string Name;
            public string Description;
        }

        public enum CartItemType { ANIMAL, FOOD };

        public int Quantity { get; set; }
        public ProductData Product { get; set; }

        public CartItemType Type { get; set; }
    }
}
