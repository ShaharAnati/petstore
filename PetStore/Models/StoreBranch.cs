﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class StoreBranch
    {
        public int Id { get; set; }

        [Display(Name = "Branch Name")]
        [MaxLength(25)]
        [Required]
        public string Name { get; set; }

        [MaxLength(25)]
        [Required]
        public string Address { get; set; }

        [Display(Name = "Latitude Coordinate")]
        [Range(29.3, 33.5)] // Israel's borders
        [Required]
        public double LatitudeCoord { get; set; }

        [Display(Name = "Longtitude Coordinate")]
        [Range(33.7, 36.3)] // Israel's borders
        [Required]
        public double LongtitudeCoord { get; set; }

        public List<Animal> Animals { get; set; }
    }
}
