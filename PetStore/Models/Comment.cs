﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Models
{
    public class Comment
    {
        public int Id { get; set; }

        public User user { get; set; }

        public string text { get; set; }
    }
}
