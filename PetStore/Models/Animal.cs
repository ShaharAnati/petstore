﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace PetStore.Models
{
    public enum Gender
    {
        [Display(Name = "Male")]
        M = 1,
        [Display(Name = "Female")]
        N = 2
    }
    public class Animal
    {

        public int Id { get; set; }

        [Required]
        [Display(Name = "Type Id")]
        public string TypeId { get; set; }
        public AnimalType Type { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(30)]
        public string Name { get; set; }

        [Range(0, 200)]
        public int Age { get; set; }

        public byte[] Image { get; set;  }

        [NotMapped]
        public IFormFile ImageFile { get; set; }

        [Required]
        public Gender Gender { get; set; }

        public string Description { get; set; }

        [Range(0, 999999)]
        public double Price { get; set; }

        [Required]
        [Display(Name = "Branch Id")]
        public int StoreBranchId { get; set; }
        public StoreBranch StoreBranch { get; set; }


        public int? OrderId { get; set; }
        public Order Order { get; set; }
        
    }
}
