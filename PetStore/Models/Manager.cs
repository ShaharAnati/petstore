﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PetStore.Models
{
    public class Manager
    {
        [Key]
        public string EmployeeId { get; set; }

        public string UserName { get; set; }

        [ForeignKey("UserName")]
        public User User { get; set; }
    }
}
