﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace PetStore.Models
{
    public class User
    {
        [Key]
        [Required]
        [MinLength(5, ErrorMessage = "Username must consist of at least 5 characters")]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        [Required]
        [MinLength(5, ErrorMessage = "Password must consist of at least 5 characters")]
        public string Password { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        public List<Comment> Comments { get; set; }
        public List<Order> Orders { get; set; }

        public Manager? Manager { get; set; }

    }
}
