﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetStore.Data;
using PetStore.Helpers;
using PetStore.Models;

namespace PetStore.Controllers
{
    public class OrdersController : Controller
    {
        private readonly PetStoreContext _context;

        public OrdersController(PetStoreContext context)
        {
            _context = context;
        }

        // GET: Orders
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var role = HttpContext?.User?.FindFirstValue(ClaimTypes.Role);
            var petStoreContext = _context.Order.Include(o => o.User);

            if (role != "Admin")
            {
                var userName = HttpContext?.User?.FindFirstValue(ClaimTypes.Name);
                petStoreContext = _context.Order.Where(order => order.UserName == userName).Include(o => o.User);
            }

            return View(await petStoreContext.ToListAsync());
        }

        public async Task<IActionResult> Search(string userName)
        {
            var searchResult = _context.Order.Where(order => order.UserName.Contains(userName) || userName == null);
            return View("Index", await searchResult.ToListAsync());
        }

        // GET: Orders/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            List<OrderItem> orderItems = new List<OrderItem>();

              var orderAnimalTotalPrice = _context.Order
                                    .Join(_context.Animal,
                                           order => order.Id,
                                           animal => animal.OrderId,
                                           (order, animal) => new
                                           {
                                               orderId = order.Id,
                                               animalId = animal.Id,
                                               animalPrice = animal.Price
                                           })
                                    .ToList().GroupBy(a => a.orderId).Select(c => new { orderId = c.Key, totalPrice = c.Sum(a => a.animalPrice) })
                                    .FirstOrDefault(order => order.orderId == id);



            List<Animal> orderAnimals = _context.Animal.Where(a => a.OrderId == id).ToList();
            orderAnimals.ForEach(animal =>
            {
                orderItems.Add(new OrderItem(animal.Name, animal.Image, animal.Price, null));
            });

            double orderFoodTotalPrice = 0;

           var orderFoods = _context.FoodToOrder.Where(f => f.OrderId == id)
                                .Join(_context.Food,
                                       orderOfFood => orderOfFood.FoodId,
                                       food => food.Id,
                                         (orderOfFood, food) => new
                                         {
                                             OrderId = orderOfFood.OrderId,
                                             Quantity = orderOfFood.Quantity,
                                             Food = food
                                         });

            orderFoods.ToList().ForEach(o =>
            {
                orderFoodTotalPrice += o.Quantity * o.Food.Price;
                orderItems.Add(new OrderItem(o.Food.Name, o.Food.Image, o.Food.Price, o.Quantity));
            });

            ViewBag.orderItems = orderItems;

            var order = await _context.Order
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);

            var price = orderAnimalTotalPrice !=null ? orderAnimalTotalPrice.totalPrice : 0;

            order.TotalPrice = price + orderFoodTotalPrice;
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // GET: Orders/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["UserName"] = new SelectList(_context.User, "UserName", "UserName");
            return View();
        }

        // POST: Orders/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,CreationDate,UserName")] Order order)
        {
            if (ModelState.IsValid)
            {
                _context.Add(order);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserName"] = new SelectList(_context.User, "UserName", "UserName", order.UserName);
            return View(order);
        }

        // GET: Orders/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order.FindAsync(id);
            if (order == null)
            {
                return NotFound();
            }
            ViewData["UserName"] = new SelectList(_context.User, "UserName", "UserName", order.UserName);
            return View(order);
        }

        // POST: Orders/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CreationDate,UserName")] Order order)
        {
            if (id != order.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(order);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OrderExists(order.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserName"] = new SelectList(_context.User, "UserName", "UserName", order.UserName);
            return View(order);
        }

        // GET: Orders/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var order = await _context.Order
                .Include(o => o.User)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (order == null)
            {
                return NotFound();
            }

            return View(order);
        }

        // POST: Orders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var order = await _context.Order.FindAsync(id);
            _context.Order.Remove(order);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool OrderExists(int id)
        {
            return _context.Order.Any(e => e.Id == id);
        }

        [Authorize]
        public async Task<IActionResult> CreateOrder()
        {
            if (ModelState.IsValid)
            {
                List<CartItem> cartFood = new List<CartItem>();
                Order order = new Order();
                order.CreationDate = DateTime.Now;
                order.UserName = HttpContext?.User?.FindFirstValue(ClaimTypes.Name);
                List<CartItem> items = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
                if (items != null)
                {
                    for (int i = 0; i < items.Count(); i++)
                    {
                        CartItem item = items.ElementAt(i);
                        if (item.Type == CartItem.CartItemType.ANIMAL)
                        {
                            if (order.Animals == null)
                            {
                                order.Animals = new List<Animal>();
                            }

                            Animal animal = await _context.Animal.FindAsync(item.Product.Id);
                            order.Animals.Add(animal);
                        }
                        else if (item.Type == CartItem.CartItemType.FOOD)
                        {
                            if (order.Foods == null)
                            {
                                order.Foods = new List<Food>();
                            }
                            Food food = await _context.Food.FindAsync(item.Product.Id);
                            order.Foods.Add(food);
                            cartFood.Add(item);
                        }
                    };
                }

                // create new cart
                Order createdOrder =_context.Add(order).Entity;

                
                await _context.SaveChangesAsync();

                List<FoodToOrder> foodToOrders = new List<FoodToOrder>();
                cartFood.ForEach(item =>
                {
                    FoodToOrder f = new FoodToOrder();
                    f.FoodId = item.Product.Id;
                    f.OrderId = createdOrder.Id;
                    f.Quantity = item.Quantity;
                    foodToOrders.Add(f);
                });

                _context.AddRange(foodToOrders);


                // set all animals as bought
                order.Animals?.ForEach(animal =>
                {
                    //animal.isBought = true;
                    animal.OrderId = createdOrder.Id;
                    _context.Animal.Update(animal);

                });
                await _context.SaveChangesAsync();


                // reset the cart
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", null);
                return RedirectToAction(nameof(Index));
            }

            return View(nameof(Index));
        }

    }
}
