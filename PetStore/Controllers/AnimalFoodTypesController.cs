﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetStore.Data;
using PetStore.Models;

namespace PetStore.Controllers
{
    public class AnimalFoodTypesController : Controller
    {
        private readonly PetStoreContext _context;

        public AnimalFoodTypesController(PetStoreContext context)
        {
            _context = context;
        }

        // GET: AnimalFoodTypes
        public async Task<IActionResult> Index()
        {
            return View(await _context.AnimalFoodType.ToListAsync());
        }

        // GET: AnimalFoodTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animalFoodType = await _context.AnimalFoodType
                .FirstOrDefaultAsync(m => m.AnimalTypeId == id);
            if (animalFoodType == null)
            {
                return NotFound();
            }

            return View(animalFoodType);
        }

        // GET: AnimalFoodTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AnimalFoodTypes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AnimalTypeId,FoodId")] AnimalFoodType animalFoodType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(animalFoodType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(animalFoodType);
        }

        // GET: AnimalFoodTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animalFoodType = await _context.AnimalFoodType.FindAsync(id);
            if (animalFoodType == null)
            {
                return NotFound();
            }
            return View(animalFoodType);
        }

        // POST: AnimalFoodTypes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AnimalTypeId,FoodId")] AnimalFoodType animalFoodType)
        {
            if (id != animalFoodType.AnimalTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(animalFoodType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnimalFoodTypeExists(animalFoodType.AnimalTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(animalFoodType);
        }

        // GET: AnimalFoodTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animalFoodType = await _context.AnimalFoodType
                .FirstOrDefaultAsync(m => m.AnimalTypeId == id);
            if (animalFoodType == null)
            {
                return NotFound();
            }

            return View(animalFoodType);
        }

        // POST: AnimalFoodTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var animalFoodType = await _context.AnimalFoodType.FindAsync(id);
            _context.AnimalFoodType.Remove(animalFoodType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AnimalFoodTypeExists(int id)
        {
            return _context.AnimalFoodType.Any(e => e.AnimalTypeId == id);
        }
    }
}
