﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PetStore.Helpers;
using PetStore.Data;
using PetStore.Models;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;

namespace PetStore.Controllers
{
    public class CartController : Controller
    {
        private readonly PetStoreContext _context;

        public CartController(PetStoreContext context)
        {
            _context = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");

            ViewBag.cart = cart;
            if (cart != null)
            {
                ViewBag.countProducts = cart.Count();
                ViewBag.total = calculateSum();
            } else
            {
                ViewBag.countProducts = 0;
                ViewBag.total = 0;
            }

            return View();
        }

        [Authorize]
        public IActionResult Add(int id, CartItem.CartItemType type)
        {
            if (SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart") == null)
            {
                List<CartItem> cart = new List<CartItem>();
                cart.Add(new CartItem { Product = getProductData(id, type), Quantity = 1, Type = type });
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            else
            {
                List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
                int index = isExistInCart(id, type);
                if (index != -1)
                {
                    if(type != CartItem.CartItemType.ANIMAL)
                    {
                        cart[index].Quantity++;
                    }
                }
                else
                {
                    cart.Add(new CartItem { Product = getProductData(id, type), Type = type, Quantity = 1 });
                }
                SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            }
            return RedirectToAction(nameof(Index));
        }


        [Authorize]
        public IActionResult Remove(int id, CartItem.CartItemType type)
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            int index = isExistInCart(id, type);
            cart.RemoveAt(index);

            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction(nameof(Index));
        }

        [Authorize]
        public IActionResult Reduce(int id, CartItem.CartItemType type)
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            int index = isExistInCart(id, type);
            cart[index].Quantity--;
            if (cart[index].Quantity == 0)
            {
                cart.RemoveAt(index);
            }
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", cart);
            return RedirectToAction(nameof(Index));
        }


        private int isExistInCart(int id, CartItem.CartItemType type)
        {
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            for (int i = 0; i < cart.Count; i++)
            {
                if (cart[i].Product.Id == id && cart[i].Type.Equals(type))
                {
                    return i;
                }
            }
            return -1;
        }


        private double calculateSum()
        {
            double sum = 0;
            List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
            cart.ForEach(item =>
            {
                sum += item.Product.Price * item.Quantity;
            });


            return sum;
        }
        private CartItem.ProductData getProductData(int id, CartItem.CartItemType type)
        {
            if (type == CartItem.CartItemType.ANIMAL)
            {
                Animal animal = _context.Animal.First(a => a.Id == id);
                if (animal != null)
                {
                    return new CartItem.ProductData { Id = animal.Id, Name = animal.Name, Image = animal.Image, Price = animal.Price, Description = animal.Description  };
                }
            }
            else if (type == CartItem.CartItemType.FOOD)
            {
                Food food = _context.Food.First(f => f.Id == id);
                if (food != null)
                {
                    return new CartItem.ProductData { Id = food.Id, Name = food.Name, Image = food.Image, Price = food.Price, Description = food.Description }; ;
                }
            }

            return null;
        }
    }
}
