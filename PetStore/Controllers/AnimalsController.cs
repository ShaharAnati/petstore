﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetStore.Data;
using PetStore.Models;
using PetStore.Helpers;
using System.Security.Claims;

namespace PetStore.Controllers
{
    public class AnimalsController : Controller
    {
        private readonly PetStoreContext _context;

        public AnimalsController(PetStoreContext context)
        {
            _context = context;
        }


        private async Task<List<Animal>> getReleventAnimals()
        {
            List<Animal> animals = await _context.Animal.ToListAsync();

            var role = HttpContext?.User?.FindFirstValue(ClaimTypes.Role);
            if (role != "Admin")
            {
                List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
                if (cart != null)
                {
                    cart.RemoveAll(item => item.Type != CartItem.CartItemType.ANIMAL);
                    animals.RemoveAll(a => cart.Exists(cartItem => cartItem.Product.Id == a.Id) || a.OrderId != null);
                }
                else
                {
                    animals.RemoveAll(a => a.OrderId != null);
                }
            }

            return animals;
        }

        // GET: Animals
        public async Task<IActionResult> Index()
        {
            List<Animal> animals = await getReleventAnimals();

            return View(animals);
        }

        public dynamic GetAll()
        {
            var role = HttpContext?.User?.FindFirstValue(ClaimTypes.Role);
            if (role != "Admin")
            {
                List<CartItem> cart = SessionHelper.GetObjectFromJson<List<CartItem>>(HttpContext.Session, "cart");
                if (cart != null)
                {
                   var cartAnimalIds = cart.Where(item => item.Type == CartItem.CartItemType.ANIMAL).Select(item => item.Product.Id );

                    return _context.Animal.Where(animal => animal.OrderId == null
                        && !cartAnimalIds.Any(id => id == animal.Id))
                        .Select(animal => new { animal.Price }).ToList();
                }
                else
                {
                    return _context.Animal.Where(animal => animal.OrderId == null)
                        .Select(animal => new { animal.Price }).ToList();
                }
            }

            return _context.Animal.Select(animal => new { animal.Price }).ToList();
        }

        public async Task<IActionResult> Search(string name, int price, Gender gender)
        {
            var searchResult = (await getReleventAnimals())
                .Where(a => (name == null  || a.Name.Contains(name)) && (price == 0 || a.Price <= price) && (a.Gender.Equals(gender) || gender == 0));
            return View("Index", searchResult);
        }

        // GET: Animals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal
                .FirstOrDefaultAsync(m => m.Id == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // GET: Animals/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["StoreBranchId"] = new SelectList(_context.Set<StoreBranch>(), "Id", "Name");
            ViewData["TypeId"] = new SelectList(_context.Set<AnimalType>(), "Id", "Id");
            return View();
        }

        // POST: Animals/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Age,OrderId,Gender,Description,Price,StoreBranchId,ImageFile,TypeId")] Animal animal)
        {
            if (ModelState.IsValid)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    animal.ImageFile.CopyTo(ms);
                    animal.Image = ms.ToArray();
                }

                _context.Add(animal);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(animal);
        }

        // GET: Animals/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal.FindAsync(id);
            if (animal == null)
            {
                return NotFound();
            }
            ViewData["StoreBranchId"] = new SelectList(_context.Set<StoreBranch>(), "Id", "Id", animal.StoreBranchId);
            return View(animal);
        }

        // POST: Animals/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Age,OrderId,Gender,Description,Price,StoreBranchId,Image,ImageFile,TypeId")] Animal animal)
        {
            if (id != animal.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        animal.ImageFile.CopyTo(ms);
                        animal.Image = ms.ToArray();
                    }
                    _context.Update(animal);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AnimalExists(animal.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(animal);
        }

        // GET: Animals/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var animal = await _context.Animal
                .FirstOrDefaultAsync(m => m.Id == id);
            if (animal == null)
            {
                return NotFound();
            }

            return View(animal);
        }

        // POST: Animals/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var animal = await _context.Animal.FindAsync(id);
            _context.Animal.Remove(animal);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AnimalExists(int id)
        {
            return _context.Animal.Any(e => e.Id == id);
        }
    }
}
