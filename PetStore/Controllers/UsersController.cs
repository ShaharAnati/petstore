﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetStore.Data;
using PetStore.Helpers;
using PetStore.Models;

namespace PetStore.Controllers
{
    public class UsersController : Controller
    {
        private readonly PetStoreContext _context;

        public UsersController(PetStoreContext context)
        {
            _context = context;
        }

        // GET: Users
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.User.ToListAsync());
        }

        public async Task<IActionResult> Search(string fullName)
        {

            var searchResult = from user in _context.User
                    where (user.FullName.Contains(fullName) || fullName == null)
                    orderby user.FullName descending
                    select user;
            return View("Index", await searchResult.ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.UserName == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // GET: Users/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }


        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(bool isManager, [Bind("UserName,Address,Email,Password,FullName")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                if (isManager) _context.Add(new Manager { EmployeeId = user.UserName, User = user });

                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User.FindAsync(id);

            var m = _context.Manager.FirstOrDefault(manager => manager.UserName == user.UserName);
            user.Manager = m;

            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string UserName, [Bind("UserName,Address,Email,Password,FullName")] User user, bool isManager)
        {
            if (UserName != user.UserName)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(user);
                    Manager m = _context.Manager.FirstOrDefault(m => m.UserName == user.UserName);
                    bool isCurrentlyManager = m != null;


                    if (isManager && !isCurrentlyManager) { _context.Add(new Manager { EmployeeId = user.UserName, User = user });
                       
                    }
                    
                    else if(isCurrentlyManager && !isManager)
                    {
                        _context.Remove(m);
                    }

                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UserExists(user.UserName))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }

        // GET: Users/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = await _context.User
                .FirstOrDefaultAsync(m => m.UserName == id);
            if (user == null)
            {
                return NotFound();
            }

            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string UserName)
        {
            var user = await _context.User.FindAsync(UserName);
            _context.User.Remove(user);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UserExists(string id)
        {
            return _context.User.Any(e => e.UserName == id);
        }

        // GET: Users/Register
        public IActionResult Register()
        {
            return View();
        }

        // POST: Users/Register
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("UserName,Address,Email,Password,FullName")] User user)
        {
            if (ModelState.IsValid)
            {
                User foundUser = _context.User.FirstOrDefault(u => u.UserName == user.UserName);

                if (foundUser == null)
                {
                    _context.Add(user);
                    await _context.SaveChangesAsync();

                    var foundUsers = from u in _context.User
                                     where u.UserName == user.UserName && u.Password == user.Password
                                     select u;

                    this.Signin(foundUsers.First());
                    return RedirectToAction(nameof(Index), "Home");
                } else {
                    ViewData["Error"] = "UserName taken";
                }
            }
            return View(user);
        }


        // GET: Users/LogIn
        public IActionResult LogIn()
        {
            return View();
        }

        // POST: Users/LogIn
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn([Bind("UserName,Password")] User user)
        {
            if (ModelState.IsValid)
            {
                var foundUsers = from u in _context.User
                                 where u.UserName == user.UserName && u.Password == user.Password
                                 select u;

                if (foundUsers.Count() != 0)
                {
                 /*   HttpContext.Session.SetString("userName", foundUsers.First().UserName);
                    HttpContext.Session.SetString("password", foundUsers.First().Password);*/

                    var manager = _context.Manager.FirstOrDefault(u => u.EmployeeId == user.UserName);

                    User x = foundUsers.First();
                    x.Manager = manager;
                    this.Signin(x);

                    return RedirectToAction(nameof(Index), "Home");
                }
                else
                {
                    ViewData["Error"] = "UserName and/or Password are incorrect";
                }
            }
            return View(user);
        }

        // Log Out
        public async Task<IActionResult> Logout()
        {

            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            // reset cart
            SessionHelper.SetObjectAsJson(HttpContext.Session, "cart", null);
            return RedirectToAction("Login");
        }

        private async void Signin(User account)
        {
            var claims = new List<Claim> {
                new Claim(ClaimTypes.Name, account.UserName),
                new Claim(ClaimTypes.Role, account.Manager != null ? "Admin" : "Client")
            };

            var claimsIdentity = new ClaimsIdentity(
                claims, 
                CookieAuthenticationDefaults.AuthenticationScheme
                );

            var authProperties = new AuthenticationProperties
            {
                /*ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10)*/
            };
            
            await HttpContext.SignInAsync(
                CookieAuthenticationDefaults.AuthenticationScheme, 
                new ClaimsPrincipal(claimsIdentity),
                authProperties);
        }

        // GET: Users/AccessDenied
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
        
}
