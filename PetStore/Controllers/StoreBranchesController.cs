﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PetStore.Data;
using PetStore.Models;

namespace PetStore.Controllers
{
    public class StoreBranchesController : Controller
    {
        private readonly PetStoreContext _context;

        public StoreBranchesController(PetStoreContext context)
        {
            _context = context;
        }

        // GET: StoreBranches
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.StoreBranch.ToListAsync());
        }

        public dynamic GetBranchCoords()
        {
            return _context.StoreBranch.Select(branch => new { branch.Name, branch.LatitudeCoord, branch.LongtitudeCoord }).ToList();
        }

        public async Task<IActionResult> Search(string name)
        {
            var searchResult = _context.StoreBranch.Where(storeBranch => storeBranch.Name.Contains(name) || name == null);
            return View("Index", await searchResult.ToListAsync());
        }

        // GET: StoreBranches/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storeBranch = await _context.StoreBranch
                .FirstOrDefaultAsync(m => m.Id == id);
            if (storeBranch == null)
            {
                return NotFound();
            }

            return View(storeBranch);
        }

        // GET: StoreBranches/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: StoreBranches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,LatitudeCoord,LongtitudeCoord")] StoreBranch storeBranch)
        {
            if (ModelState.IsValid)
            {
                _context.Add(storeBranch);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(storeBranch);
        }

        // GET: StoreBranches/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storeBranch = await _context.StoreBranch.FindAsync(id);
            if (storeBranch == null)
            {
                return NotFound();
            }
            return View(storeBranch);
        }

        // POST: StoreBranches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,LatitudeCoord,LongtitudeCoord")] StoreBranch storeBranch)
        {
            if (id != storeBranch.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(storeBranch);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoreBranchExists(storeBranch.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(storeBranch);
        }

        // GET: StoreBranches/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var storeBranch = await _context.StoreBranch
                .FirstOrDefaultAsync(m => m.Id == id);
            if (storeBranch == null)
            {
                return NotFound();
            }

            return View(storeBranch);
        }

        // POST: StoreBranches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var storeBranch = await _context.StoreBranch.FindAsync(id);
            _context.StoreBranch.Remove(storeBranch);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StoreBranchExists(int id)
        {
            return _context.StoreBranch.Any(e => e.Id == id);
        }
    }
}
