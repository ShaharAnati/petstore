﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PetStore.Models;
using PetStore.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using TweetSharp;

namespace PetStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PetStoreContext _context;


        public HomeController(ILogger<HomeController> logger, PetStoreContext context)
        {
            _logger = logger;
            _context = context;
        }

        [Authorize]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Privacy()
        {
          /*  if(HttpContext.Session.GetString("userName") == null)
            {
                return RedirectToAction("Login", "Users");
            }*/

            return View();
        }

        [HttpPost]
        public ActionResult Index(Comment tweet)
        {
            string key = "bXPccOFylqkeQqSgjFfsEhqqN";
            string secret = "IY5UE4TXXJdLxIokHfLDGD8hkCnX0B4PK70HtBYefE5E5sTfo3";
            string token = "1406209026057199622-iTjUjB3FJeCzDywKPWlI9lH1IIV7Sq";
            string tokenSecret = "rUetJw7jTbqLlBosjMIpgxNpRtXBoOFwA3uUPH87sdJkN";

            var service = new TweetSharp.TwitterService(key, secret);
            service.AuthenticateWith(token, tokenSecret);

            string message = tweet.text;

            service.SendTweet(new TweetSharp.SendTweetOptions
            {
                Status = message
            });

            tweet.text = "";
            return RedirectToAction("Index", "Home");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
