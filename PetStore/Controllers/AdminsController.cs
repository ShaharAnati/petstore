﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PetStore.Data;
using PetStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStore.Controllers
{
    public class AdminsController : Controller { 

        private readonly PetStoreContext _context;

    public AdminsController(PetStoreContext context)
    {
        _context = context;
    }
    
        [Authorize(Roles = "Admin")]
        public IActionResult Index()
        {
            ViewData["StoreBranchesCount"] =  _context.StoreBranch.ToList().Count();
            ViewData["AnimalsCount"] = _context.Animal.ToList().Count();
            ViewData["FoodsCount"] = _context.Food.ToList().Count();
            ViewData["OrdersCount"] = _context.Order.ToList().Count();
            ViewData["UsersCount"] = _context.User.ToList().Count();
            ViewData["AnimalTypesCount"] = _context.AnimalType.ToList().Count();


            return View();
        }

        [Authorize(Roles = "Admin")]
        public IActionResult Statistics()
        {
            var avgPriceByType = _context.Animal.GroupBy(a => a.TypeId).Select(c => new { animalType = (c.Key), avg = c.Sum(a=> a.Price)/c.Count() }).ToList();
            var avgPriceByTypeData = JsonConvert.SerializeObject(avgPriceByType);
            ViewData["avgPriceByTypeData"] = avgPriceByTypeData;


            var ordersByStoreBranches = _context.Animal
                .Join(_context.StoreBranch,
                        animal => animal.StoreBranchId,
                        storeBranch => storeBranch.Id,
                          (animal, storeBranch) => new
                          {
                              animalId = animal.Id,
                              storeBranchId = animal.StoreBranchId,
                              storeBranchName = storeBranch.Name,
                              orderId = animal.OrderId
                          })
                .ToList()
                .GroupBy(a => a.storeBranchId)
                .Select(c => new { storeBranchName = c.First(i=> i.storeBranchId == c.Key).storeBranchName, 
                                   ordersCount = c.Where(i => i.orderId != null).GroupBy(i => i.orderId).Count() })
                .ToList();

            

            var ordersByStoreBranchData = JsonConvert.SerializeObject(ordersByStoreBranches);
            ViewData["ordersByStoreBranchData"] = ordersByStoreBranchData;  

            return View();
            
           
        }
    }
}
