﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using PetStore.Models;

namespace PetStore.Data
{
    public class PetStoreContext : DbContext
    {
        public PetStoreContext (DbContextOptions<PetStoreContext> options)
            : base(options)
        {
        }

        
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<FoodToOrder>().HasKey(c => new { c.FoodId, c.OrderId });
            modelBuilder.Entity<AnimalFoodType>().HasKey(c => new { c.AnimalTypeId, c.FoodId });
            modelBuilder.Entity<Animal>().Property(p => p.Gender)
                .HasConversion(v => v.ToString(), v => (Gender)Enum.Parse(typeof(Gender), v));

        
        }
        
        public DbSet<PetStore.Models.Food> Food { get; set; }
        public DbSet<PetStore.Models.User> User { get; set; }
        public DbSet<PetStore.Models.Animal> Animal { get; set; }
        public DbSet<PetStore.Models.AnimalFoodType> AnimalFoodType { get; set; }
        public DbSet<PetStore.Models.AnimalType> AnimalType { get; set; }
        public DbSet<PetStore.Models.StoreBranch> StoreBranch { get; set; }
        public DbSet<PetStore.Models.Comment> Comment { get; set; }
        public DbSet<PetStore.Models.Order> Order { get; set; }
        public DbSet<PetStore.Models.FoodToOrder> FoodToOrder { get; set; }
        public DbSet<PetStore.Models.Manager> Manager { get; set; }
    }
}
